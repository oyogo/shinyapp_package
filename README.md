shiny.package   

packaging shiny Apps  

You can install shinyapp.package from gitlab with:

`` `devtools::install_gitlab('oyogo/shinyapp_package')` ``  

And to load the library please use:  

`` `library(shinyapp.package)` ``  

To launch the app run:

`` `run_app()` ``







