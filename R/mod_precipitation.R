

# Module UI

#' @title   mod_precipitation_ui and mod_precipitation_server
#' @description  A shiny Module.
#'
#' @param id shiny id
#' @param input internal
#' @param output internal
#' @param session internal
#'
#' @rdname mod_precipitation
#'
#' @keywords internal
#' @export
#' @importFrom shiny NS tagList


mod_precipitation_ui <- function(id) {
ns <- NS(id)

tagList(

tabItem(tabName = "precipitation",

        fluidRow(
          column(width = 6, box(color = "teal",ribbon = TRUE,title_side = "top right",title = "Line plot",plotOutput(ns("perf.rain")), tags$span(style = "color:black", "The dotted line shows the cumulative rainfall for the selected year1(i.e 1998)
                                                                         and the solid line while the solid line shows the cumulative rainfall for selected year2(i.e 2017)."))),

          ),
)
)

}


# Module Server

#' @rdname mod_precipitation
#' @export
#' @keywords internal

mod_precipitation_server <- function(input, output,session) {

  ns <- session$ns


  Time <- c(2001:2010)
  Tmean <- c(12.06, 11.78,11.81,11.72,12.02,12.36,12.03,11.27,11.33,11.66)
  Prec <- c(737.11,737.87,774.95,844.55,764.03,757.43,741.17,793.50,820.42,796.80)

  dat <- data.frame(Time,Tmean,Prec)

  output$perf.rain <- renderPlot({



    ggplot(dat, aes(x=Time, y = Prec)) + geom_line()

  })

}

