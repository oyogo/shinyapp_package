% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/mod_precipitation.R
\name{mod_precipitation_ui}
\alias{mod_precipitation_ui}
\alias{mod_precipitation_server}
\title{mod_precipitation_ui and mod_precipitation_server}
\usage{
mod_precipitation_ui(id)

mod_precipitation_server(input, output, session)
}
\arguments{
\item{id}{shiny id}

\item{input}{internal}

\item{output}{internal}

\item{session}{internal}
}
\description{
A shiny Module.
}
\keyword{internal}
